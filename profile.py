"""Simple single node profile to enable PTP synchronization from Powder switches.
"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import IG extensions
import geni.rspec.igext as ig
# Emulab specific extensions.
import geni.rspec.emulab as emulab
# Spectrum specific extensions.
import geni.rspec.emulab.lanext as lanext

# Create a portal context, needed to defined parameters
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

type = "d740"
image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"

node1 = request.RawPC("node1")
node1.hardware_type = type
node1.disk_image = image
ptpiface = node1.addInterface()
#ptpiface.component_id = "eth4"
#ptpiface.addAddress(pg.IPv4Address("10.13.1.1", "255.255.255.0"))

#
# Enable PTP. The PTP method will tell the connected switch port to provide
# PTP. The specified startup script will take care of installing Linux PTP
# tools and enabling a PTP hardware clock (PHC) to synchronize with the
# interface and tie the system clock to that PHC.
#
ptpiface.PTP()
node1.addService(pg.Execute(shell="sh", command="sudo sh /local/repository/scripts/startup.sh"))

#
# XXX need a link right now in order for PTP to be setup.
# So we add a rando second node and connect it with the node in question.
#
node2 = request.RawPC("node2")
#node2.hardware_type = type
otheriface = node2.addInterface()
#otheriface.component_id = "eth4"
#otheriface.addAddress(pg.IPv4Address("10.13.1.2", "255.255.255.0"))

# Link the nodes
link = request.Link("link", members = [ptpiface, otheriface])

# Print the RSpec to the enclosing page.
pc.printRequestRSpec(request)
